## Instalacje

node -v
v16.13.1

npm -v
6.14.6

git --version
git version 2.31.1.windows.1

// https://git-scm.com/download/mac
brew install git

code -v
1.63.2


docker --version
Docker version 20.10.12, build e91ed57
docker ps

docker run -d -p 80:80 docker/getting-started
visit http://localhost/
docker rm -f e0dddc835727

https://golb.hplar.ch/2020/05/docker-windows-home-2004.html


## GIT Syncing changes from upstream

<!-- Clone do katalogu ekonomiczny-krakow-nodejs -->
git clone https://bitbucket.org/<WASZ_LOGIN>/ekonomiczny-krakow-nodejs.git ekonomiczny-krakow-nodejs
<!-- lub do biezacego katalogu -->
git clone https://bitbucket.org/<WASZ_LOGIN>/ekonomiczny-krakow-nodejs.git .

cd ekonomiczny-krakow-nodejs

git remote add trener https://bitbucket.org/ev45ive/ekonomiczny-krakow-nodejs.git

<!-- pobierz najnowze zmiany od trenera (tzw. upstream)  -->
git pull trener master


## Git proxy
git config --global --unset http.proxy
git config --global --unset https.proxy

## NPM install global
npm i -g http-server

C:\Users\PC\AppData\Roaming\npm\http-server -> C:\Users\PC\AppData\Roaming\npm\node_modules\http-server\bin\http-server
+ http-server@14.1.0
added 18 packages from 14 contributors, removed 2 packages and updated 8 packages in 3.083s

http-server -p 8080

### Linux / Mac  global packages no sudo
https://stackoverflow.com/questions/14803978/npm-global-path-prefix

~/.npmrc :
npm config set prefix /Users/[your username]/.npm-packages/bin

.bash_profile :
export PATH=$PATH:/Users/[your username]/.npm-packages/bin
